.include "asminc/lynx.inc"

;============================================================================
; Game dependent data supplied by the application programmer

PLAYER_DATA_SIZE = 4
MAX_PLAYERS = 2
GAME_ID = 0

.bss
restartcount: .res 1
restartflag: .res 1
joystick: .res MAX_PLAYERS
switch: .res MAX_PLAYERS
oldjoystick: .res MAX_PLAYERS
oldswitch: .res MAX_PLAYERS
PlayerNumber: .res 1
NumberOfPlayers: .res 1

.code
showlogonscreen:
	rts

restart:
	rts

sysIntTable:
transmissionErrorVector:
msgOverflowVector:
readMsgVector:
         rts

;============================================================================
; Bit definitions

DISPLAY_BUFSIZE	= $1FE0
OPTION1_BUTTON	= %00001000
OPTION2_BUTTON	= %00000100
INNER_BUTTON	= %00000010
OUTER_BUTTON	= %00000001
RESTART_BUTTON	= OPTION1_BUTTON
FLIP_BUTTON	= OPTION2_BUTTON

PAUSE_SWITCH	= %00000001

; TIM_CONTROLA control bits
ENABLE_INT	= %10000000
RESET_DONE	= %01000000
ENABLE_RELOAD	= %00010000
ENABLE_COUNT	= %00001000
AUD_CLOCK_MASK	= %00000111
; Clock settings
AUD_LINKING	= %00000111
AUD_64		= %00000110
AUD_32		= %00000101
AUD_16		= %00000100
AUD_8		= %00000011
AUD_4		= %00000010
AUD_2		= %00000001
AUD_1		= %00000000

; TIM_CONTROLB control bits
TIMER_DONE	= %00001000
LAST_CLOCK	= %00000100
BORROW_IN	= %00000010
BORROW_OUT	= %00000001

; Here's the IODIR and IODAT bit definitions
AUDIN_BIT	= %00010000	; note that there is also the address AUDIN
READ_ENABLE	= %00010000	; same bit for AUDIN_BIT
RESTLESS	= %00001000
NOEXP		= %00000100	; If set, redeye is not connected
CART_ADDR_DATA	= %00000010
CART_POWER_OFF	= %00000010	; same bit for CART_ADDR_DATA
EXTERNAL_POWER	= %00000001

; Here's the SERCTL bit definitions when writing
TXINTEN		= %10000000
RXINTEN		= %01000000
;unused		= %00100000	; Leave unused bit 0 for future compatability
PAREN		= %00010000
RESETERR	= %00001000
TXOPEN		= %00000100
TXBRK		= %00000010
PAREVEN		= %00000001
; Here's the SERCTL bit definitions when reading
TXRDY		= %10000000
RXRDY		= %01000000
TXEMPTY		= %00100000
PARERR		= %00010000
OVERRUN		= %00001000
FRAMERR		= %00000100
RXBRK		= %00000010
PARBIT		= %00000001

; Here are the DISPCTL bit definitions
DISP_COLOR	= %00001000	; must be set to 1
DISP_FOURBIT	= %00000100	; must be set to 1
DISP_FLIP	= %00000010
DMA_ENABLE	= %00000001	; must be set to 1

; These are the MAPCTL flag definitions
TURBO_DISABLE	= %10000000
VECTOR_SPACE	= %00001000	; 1 maps RAM into specified space
ROM_SPACE	= %00000100
MIKEY_SPACE	= %00000010
SUZY_SPACE	= %00000001


;============================================================================
; COMPILE TIME DEFS

.IFNDEF REDEYE_SLOWNESS
REDEYE_SLOWNESS	= 0
.ENDIF

STDCOM		=	PAREN+TXOPEN	; default communication params

COMLINK_TIMER	=	1	; used by message manager

MSG_GAP_Divider	=	AUD_4
MSG_GAP_TIMEOUT =	380/4	; actual value stuffed into timer

TxToRx_Divider	=	AUD_1
TxToRx_TIMEOUT	=	176-60	; actual value stuffed into timer for fast irq

.IF REDEYE_SLOWNESS = 0
TxToRx_SLOW	=	176-60	; actual value stuffed into timer for fast irq
.ENDIF
.IF REDEYE_SLOWNESS = 1
TxToRx_SLOW	=	176-30	; actual value stuffed into timer for fast irq
.ENDIF
.IF REDEYE_SLOWNESS = 2
TxToRx_SLOW	=	176-15	; actual value stuffed into timer for fast irq
.ENDIF
.IF REDEYE_SLOWNESS = 3
TxToRx_SLOW	=	176-0	; actual value stuffed into timer for fast irq
.ENDIF

InterMsg_Divider =	AUD_4
InterMsg_TIMEOUT =	1000/4

Long_Divider	=	AUD_64
Long_TIMEOUT	=	16000/64

; RxMsgStat bit definitions
MSG_CHECKSUM_ERR	= $80
MSG_SIZE_ERR		= $40
;PARERR			= $10		; same as SERCTL bit
;OVERRUN		= $08		; same as SERCTL bit
;FRAMERR		= $04		; same as SERCTL bit


; === controls.mac ==========================================================
; 
; Apple Joystick / Switches Input Macros  --  Handy Software


	.MACRO GETSWITCH
	  LDA SWITCHES
	.ENDMACRO


; Display Macros  --  Handy Software



; The INITEOF macro is used to initialize the end-of-frame handler.
; You should invoke this macro only once, any time before your first usage
; of WAITEOF.
.IFDEF	EOF_USER
		.MACRO	INITEOF
		JSR	InitEOF
		.ENDMACRO
.ENDIF


; The INITEOL macro is used to initialize the end-of-line handler.
; You should invoke this macro only once, any time before your first usage
; of WAITEOL.
.IFDEF	EOL_USER
		.MACRO	INITEOL
		JSR	InitEOL
		.ENDMACRO
.ENDIF



; The WAITEOL macro allows the programmer to wait until the display
; hardware has just finished displaying the end-of-line display pixel.
; There's a short while (many pixels of time) before the
; hardware starts working on the first pixel of the next display line, so
; after end-of-line it's safe to do stuff like change the color
; palette and things like that there
;??? Note that this macro currently provides only a close approximation of
;??? EOL timing.  The EOL interrupt handler needs a bit of fine-tuning to
;??? get the timing just right
		.MACRO	WAITEOL

		LDA	#DISPLAY_EOLFLAG
		TRB	DisplayFlags
systemp:		.=	*
		BIT	DisplayFlags
		BEQ	systemp
		.ENDMACRO


		.MACRO	WAITNEOL
systemp:		.=	*
		BIT	DisplayFlags
		BVC	systemp		; presumes that EOLFLAG equals $40
		.ENDMACRO



; The FLIP macro does everything you need to flip the display 180 degrees.
; The current orientation of the display should be transparent to your
; code unless you happen to be rendering directly into the display.  You
; can check the DISPLAY_SWAPFLAG of the DisplayFlags variable to discover
; whether or not the display is currently flipped.
		.MACRO	FLIP
		LDA	DISPCTL_RAM
		EOR	#DISP_FLIP
		STA	DISPCTL_RAM
		LDA	SPRSYS_RAM
		EOR	#%00001000
		STA	SPRSYS_RAM
		STA	SPRSYS
		.ENDMACRO




; The DISPLAY macro, after flipping DisplayBuffer as appropriate to the
; state of FLIP, tells Handy hardware that DisplayBuffer is the buffer
; to be displayed
		.MACRO	DISPLAY
		.local Skip1
		.local Skip2
		LDA	DISPCTL_RAM
                STA	DISPCTL
		AND	#DISP_FLIP
		BEQ	Skip1
		LDA	DisplayBuffer
		CLC
		ADC	#<DISPLAY_BUFSIZE-1
		TAX
		LDA	DisplayBuffer+1
		ADC	#>DISPLAY_BUFSIZE-1
		BRA	Skip2
Skip1:
		LDX	DisplayBuffer
		LDA	DisplayBuffer+1
Skip2:
		DISP_AX
		.ENDMACRO


; The DISP_AX and DISP_AY macros set up the address contained in
; A (high-byte) and X or Y (low byte) as the next buffer to be displayed
; after the next end-of-frame
		.MACRO	DISP_AX
		PHP
		SEI
		STX	DISPADRL
		STA	DISPADRH
		PLP
		.ENDMACRO

		.MACRO	DISP_AY
		PHP
		SEI
		STY	DISPADRL
		STA	DISPADRH
		PLP
		.ENDMACRO




; The HOFF and VOFF macros set up the HOFF and VOFF for your next sprite
; display list rendering.
; HOFF8 and VOFF8 are used with 8-bit positioning where your offset value
; is in A (the upper byte is set to zero for you)
;
; HOFF16 and VOFF16 are used with 16-bit positioning where the lower byte of
; your offset value is in A, the upper byte is in Y
		.MACRO	HOFF8
		STA	HOFFL
		.ENDMACRO

		.MACRO	VOFF8
		STA	VOFFL
		.ENDMACRO

		.MACRO	HOFF16
		STA	HOFFL
		STY	HOFFH
		.ENDMACRO

		.MACRO	VOFF16
		STA	VOFFL
		STY	VOFFH
		.ENDMACRO



; If AUTO_TIMEOUT_USER is defined, then add body to the TIMEOUT macro 
	  .IFDEF AUTO_TIMEOUT_USER
	    .MACRO TIMEOUT
            .local Skip1
            .local Skip2
            .local Skip3

	    LDA SWITCHES
	    AND #PAUSE_SWITCH
	    ORA JOYSTICK
	    BEQ Skip3		; If no bits set, do nothing
	    RESET_TIMEOUT

Skip3:
	    INC TimeoutCount	 ;-- Has TimeoutCount counted to $8000 yet?
	    BPL Skip1
	    STZ TimeoutCount
	    INC TimeoutCount+1
	    BPL Skip1
	    STZ TimeoutCount+1

	    LDA IODAT	 	;-- TimeoutCount hit $8000.  Power plugged in?
	    AND #EXTERNAL_POWER
	    BEQ Skip2	; No power cord, so go turn off the juice

	    INC TimeoutCount+2	;-- Plugged in.  Let above count up many times
	    LDA TimeoutCount+2
	    CMP #13	; Lucky 13!
	    BLT Skip1
Skip2:
	    STZ SYSCTL1	; Time's up, turn power off
Skip1:
	    .endmacro
	  .ELSE
	    .MACRO TIMEOUT
	    .endmacro
	  .ENDIF

; Tue Sep 18 16:53:24 1990
; == sprite.mac ============================================================
;
; Sprite Rendering Macros  --  Handy Software



; The WAITSUZY macro allows us to wait for Suzy to be finished.
; 16-Jan-89 - SHL - modified to wait until SUZY is finished by asking her
; politely if she is done yet.
; NOTE:  this macro now presumes that the SPRITEWORKING bit of SPRSYS
; is bit 0.

;	.IFDEF NO_RICOH_PATCH
;
;		.MACRO	WAITSUZY
;??1:	.= *
;		STZ	CPUSLEEP
;		LDA	SPRSYS		; check to see if SUZY is done
;		LSR	A		; get bit 0 into carry
;		BCS	??1		; nope, let's wait some more
;		STZ	SDONEACK
;		.ENDMACRO
;
;	.ELSE
;
;		.MACRO	WAITSUZY
;??1:	.= *
;		lda #SUZY_SPACE
;		tsb MAPCTL		; disable SUZY addresses
;		STZ	CPUSLEEP
;		trb MAPCTL		; reenable SUZY addresses
;		LDA	SPRSYS		; check to see if SUZY is done
;		LSR	A		; get bit 0 into carry
;		BCS	??1		; nope, let's wait some more
;		STZ	SDONEACK
;		.ENDMACRO
;
;	.ENDIF ;DEF NO_RICOH_PATCH





; Wednesday 27-Jun-90 11:49:43
; == sys.mac ===============================================================
;
; System Macros  --  Handy Software
;

; HBRK macro forces a break on a Howard board.  On a chip system, will be
; executed as one-byte one-cycle NOP.
	;.MACRO HBRK
	;.HS 13
	;.endmacro


; Do some needed hardware initialization to get system into known state

	LDA #DISP_COLOR+DISP_FOURBIT+DMA_ENABLE
	STA DISPCTL_RAM

	LDA #%00001000		; the equates have been defined to prefer this
	STA SPRSYS_RAM
	STA SPRSYS

	LDA #VECTOR_SPACE	; map in all RAM except for CPU vectors
	STA MAPCTL

	;------	Set parallel CART_ADDR_DATA and RESTLESS as output and high
	;------ If RAMCART_USER, set AUDIN (READ_ENABLE) as output and high
	;------	all others as input
 .IFDEF RAMCART_USER
	LDA #CART_ADDR_DATA+RESTLESS+READ_ENABLE
 .ELSE
	LDA #CART_ADDR_DATA+RESTLESS
 .ENDIF
	STA IODAT
	STA IODIR_RAM
	STA IODIR

	LDA #TXOPEN	; Turn off serial IRQs before starting
	STA SERCTL	;  and make sure serial port is open-collector

	  .IFDEF AUTO_TIMEOUT_USER
		STZ TimeoutCount+1
		STZ TimeoutCount+2
	  .ENDIF




	.MACRO RESET_TIMEOUT
		STZ TimeoutCount+1	; else reset timeout counter
		STZ TimeoutCount+2
	.ENDMACRO


; Friday 23-Mar-90 11:51:53
; === comlink_variables.src =================================================
;
; Intermediate level handler for Redeye  --  Handy Software
;

MASK_SIZE	= (MAX_PLAYERS+7)/8	; size of bit masks based on # players

LOGON_MSG_SIZE	= 4+MASK_SIZE		; size of logon message

 .IF PLAYER_DATA_SIZE+1>LOGON_MSG_SIZE
MAX_DATA_SIZE	= PLAYER_DATA_SIZE+1	; size of largest message
 .ELSE
MAX_DATA_SIZE	= LOGON_MSG_SIZE	; size of largest message
 .ENDIF

; ===========================================================================


start_of_comlink_variables:

; ===========================================================================

; logon + normal communication variables

XmitBuffer:	.res MAX_DATA_SIZE
ActvPlrMask	= XmitBuffer+2
LogonGameNumber	= ActvPlrMask+MASK_SIZE
; XmitBuffer is where all outgoing data is placed before calling message
; manager send routine
; ActvPlrMask is a bit mask used during logon to verify that everyone thinks
; the same players are logged in
; LogonGameNumber is used during logon to verify all units want to play
; the same game


comlink_semaphore:	.res 1
; comlink_semaphore prevents a high level interrupt from disturbing another
; one in process


; ===========================================================================
.segment "LOGON_BSS"
; logon only variables

PlayerHeard:	.res MAX_PLAYERS
; PlayerHeard array has an entry for each player during logon, set to a high
; number when that player is heard from, and counts down during every message
; transaction - the player is assumed to have dropped out or changed number
; if his entry reaches zero

inconsistancy:	.res 1
;This is set to 40 whenever the incoming mask disagrees with the local
;mask and whenever we pack down. It is decremented every transmission
;(We do not fiddle it on collisions because player 0 never experiences
;a collision)
;Logon can only terminate when this is zero and all the players have
;been heard from recently

logon_state:	.res 1
; 0 means normal logon
; 2 means too many players
; 4 means terminating logon (master does this)
; 6 finish logon for real when timer done
; 8 means checking for redeye busy before logon

timer_high_order_countdown: .res 1

random_data_size = 5
random_data:	.res random_data_size ; The clocks are scrambled into this every message.
; Thus this value will be different every time in every lynx during logon

index_into_random: .res 1

LogonInProgress:	.res 1
EndLogonRequest:	.res 1

; ===========================================================================
.bss
; After login redeye variables

.IFDEF VAR_SIZE_DATA
PlayerDataSize0:	.res MAX_PLAYERS	; sizes of messages for even sequence data
PlayerDataSize1:	.res MAX_PLAYERS	; sizes of messages for odd sequence data
.ENDIF

PlayerFlag0:	.res MAX_PLAYERS	; message receipt flags for even sequence data
PlayerFlag1:	.res MAX_PLAYERS	; message receipt flags for odd sequence data

.IFDEF VAR_SIZE_DATA
OutGoingSize:	.res 1		; size of outgoing data
.ENDIF
OutGoingFlag:	.res 1		; flag that we've sent our data
OutGoingData:	.res PLAYER_DATA_SIZE	; next outgoing piece of data
Sequence:	.res 1		; what sequence am I supposed to send next
Seq:		.res 1		; outside IRQ, what sequence data am I using
WhosNext:	.res 1		; who is supposed to send next
WhosReTxReq:	.res 1		; who asked for resend? (0=no-one)
ReTxSequence:	.res 1		; what sequence is requester on
ReTxMask:	.res MASK_SIZE	; mask for players to re-transmit data for
RxMask:		.res MASK_SIZE	; who have I not received data from
LongTimeoutFlag:	.res 1		; flag for long timeout after send request

end_of_comlink_erasable_variables:

PlayerData0:	.res (PLAYER_DATA_SIZE*MAX_PLAYERS)	; even sequence data
PlayerData1:	.res (PLAYER_DATA_SIZE*MAX_PLAYERS)	; odd sequence data

; == comlink_logon.src =====================================================
;
; Intermediate level handler for Redeye  --  Handy Software
; The logon process calls routines in comlink.src

; The algorithm for logon is as follows. Each machine speaks its player number
; in order after random delays.  If it hears another machine with the same
; player number it shifts its number to an unused number. (Or to a number it
; thinks is unused.)

; If it hears a machine whose number cyclically precedes it speak it sets
; itself to speak after a random delay of 3 to 12 msecs.  For any other event
; it sets itself to speak after a random delay of 16 to 32 msec

; Thus when all is in order each machine speaks its number cyclically in
; rapid succession


maximium_logon_message_number	= 2
; at present only two logon messages, zero and two.

; tolerance_of_silent_player	= 10+10*{MAX_PLAYERS-1}
tolerance_of_silent_player	= 40
; This is the number of transactions that can occur without a silent
; player being logged out.

INCONSISTANCY_PRESET	= 48-MAX_PLAYERS-MAX_PLAYERS
; This is the countdown value that inconsistancy gets set to whenever
; something is wrong

logon_timer		= 5


; ===========================================================================
;
; The portion of comlink code to handle logon



send_logon_message:
	sta XmitBuffer+1
	lda #LOGON_MSG_SIZE
	jmp sendLengthMessage


too_many_players:	; time out while we have no valid player number
	jsr decrement_everyone
	jsr pick_new_player_number
	jsr set_timer_16_32_millisec
	jmp exit_logon_timer_irq_service


I_have_no_player_number:	; message received while we have no player number
	ldx RxMsg+1
	jsr force_active
	jsr decrement_everyone
	jsr pick_new_player_number
	lda EndLogonRequest
	bne @00
	jmp clear_semaphore_rts

@00:	jmp finish_logon_next_timeout


pick_new_player_number:
	ldx #0
@00:	  lda PlayerHeard,x
	  beq @10
	  inx
	  cpx #MAX_PLAYERS
	 bne @00

	rts	; carry set from cpx - signal failure to find number

@10:	jsr set_number_and_force_active	; got a new number
	stz logon_state			; back to normal login

	clc	; signal that we've got a new number
	rts


invalidate_my_player_number:
	stz PlayerNumber
	stz NumberOfPlayers
	lda #2
	sta logon_state
	jsr set_timer_16_32_millisec
	jmp clear_semaphore_rts


end_logon_right_now:
	ldx PlayerNumber	; reassign my player number after login
	bra @01
@00:	  lda PlayerHeard,x	
	  bne @01
	  dec PlayerNumber
@01:	  dex
	 bpl @00

	jsr disconnect_redeye
	jmp exit_logon_timer_irq_service


disconnect_redeye:
	 php
	  sei

	  lda #RESET_DONE
	  sta TIM4CTLA	; kill redeye timer
	  sta TIM1CTLA	; kill timer 1
	  sta TIM5CTLA	; kill timer 5

	  lda #PAREN+RESETERR+TXOPEN+PAREVEN	; disable serial ints
	  sta SERCTL

	  lda #SERIAL_INTERRUPT+TIMER1_INTERRUPT+TIMER5_INTERRUPT	; clear pending ints
	  sta INTRST

	 plp

	jsr ZeroComlinkVariables

 .IF REDEYE_SLOWNESS > 0
	lda #ENABLE_RELOAD+ENABLE_COUNT+REDEYE_SLOWNESS
	sta Baud_Patch+1
	lda #ENABLE_INT+ENABLE_COUNT+TxToRx_Divider+REDEYE_SLOWNESS
	sta TxToRx_Patch+1
	lda #ENABLE_INT+ENABLE_COUNT+MSG_GAP_Divider+REDEYE_SLOWNESS
	sta MSG_GAP_Patch+1
	lda #ENABLE_INT+ENABLE_COUNT+InterMsg_Divider+REDEYE_SLOWNESS
	sta InterMsg_Patch+1
	lda #TxToRx_SLOW-1
	sta TxToRx_Patch1+1
 .ENDIF ; REDEYE_SLOWNESS > 0

	rts



send_end_logon_message:
	lda NumberOfPlayers
	beq @10			; nobody else?
	lda #2
	sta XmitBuffer
	lda XmitBuffer+1
	dea
	beq @10
	jsr send_logon_message
	jsr wait_16_milliseconds
	bra @11

@10:	jsr finish_logon_next_timeout
@11:	jmp ply_return


end_logon_command:
	ldx logon_state
	jmp (@e0,x)
@e0:	.addr @A2
	.addr @A0
	.addr @A2
	.addr @A2
	.addr @A0

@A0:	stz PlayerNumber
	stz NumberOfPlayers
@A2:
;	This is a command from someone to terminate logon
	lda #TIMER_DONE
	sta TIM5CTLB
	lda RxMsg+1
	sta timer_high_order_countdown

finish_logon_next_timeout:
	lda #6
	sta logon_state
wait_16_milliseconds:
	lda #255
	sta TIM5CNT
	stz TIM5CTLB	;start timer
	jmp clear_semaphore_rts


randomise:
	ldy index_into_random	; get random numbers by mixing in values from
	lda random_data,y	;  the horizontal and vertical screen timers
	adc TIM0CNT
	dey
	bpl @00
	ldy #random_data_size-1
@00:	adc random_data,y
	sta random_data,y
	adc TIM2CNT
	dey
	bpl @01
	ldy #random_data_size-1
@01:	adc random_data,y
	sta random_data,y
	sty index_into_random
	rts


decrement_everyone_else:
	ldx PlayerNumber
	jsr force_active

decrement_everyone:
	ldx #MAX_PLAYERS-1
@04:	  lda PlayerHeard,x
	  beq @14
	  dec PlayerHeard,x
	  bne @14
	  stz EndLogonRequest	; removing player - cancel end request
	  lda #INCONSISTANCY_PRESET
	  sta inconsistancy	; set inconsistancy
	  lda power_of_two_table,x
	  trb ActvPlrMask	; remove knowledge of player
 .IF MAX_PLAYERS>8
	  lda power_of_two_table+8,x
	  trb ActvPlrMask+1
 .ENDIF
@14:	  dex
	 bpl @04
	rts


set_number_and_force_active:
	stx PlayerNumber	; fall into force_active

force_active:
	lda #tolerance_of_silent_player
	sta PlayerHeard,x
	lda power_of_two_table,x
	tsb ActvPlrMask
 .IF MAX_PLAYERS>8
	lda power_of_two_table+8,x
	tsb ActvPlrMask+1
 .ENDIF
	rts


set_timer_16_32_millisec:
	lda #1
	sta timer_high_order_countdown

	jsr randomise
	ora #$7				; 16 to 32 msec

set_timer_entry:
	sta TIM5CNT	;set timer
	stz TIM5CTLB	;start timer
	rts


set_timer_500_msec:
	lda #31
	sta timer_high_order_countdown
	bra set_timer_entry


timer_collided:
	lda #$30			; equals three milliseconds
	sta TIM5CNT
	stz TIM5CTLB	;restart timer
;	A problem with using the timer in one shot mode is that if it
;	ever times out and is not restarted the whole process dies.
	jmp ply_return


logon_timer_irq_service:
	lda #1
	tsb comlink_semaphore
	bne timer_collided
	lda timer_high_order_countdown
	beq dispatch_timer_irq
	dec timer_high_order_countdown
	;START_ONESHOT logon_timer, 255
	lda	#255-1		; timer runs for count+1 clocks
	sta	TIM5CNT
	stz	TIM5CTLB	; reset the timer-done bit

exit_logon_timer_irq_service:
	stz comlink_semaphore
ply_return:
	sec	; Interrupt was handled
	rts

dispatch_timer_irq:
	cli
	ldx logon_state
	jmp (@e0,x)
@e0:	.addr send_normal_logon_message
	.addr too_many_players
	.addr send_end_logon_message
	.addr end_logon_right_now
	.addr send_normal_logon_message


send_normal_logon_message:
	stz logon_state

	lda inconsistancy
	beq @14
	dec inconsistancy

@14:	lda PlayerNumber
	jsr send_logon_message

	jsr decrement_everyone_else
	jsr set_timer_16_32_millisec

	ldy #$FF		; compute NumberOfPlayers
	ldx #MAX_PLAYERS-1
@06:	  lda PlayerHeard,x
	  beq @19
	  iny
	  cmp #tolerance_of_silent_player-MAX_PLAYERS-2
	  bcc exit_logon_timer_irq_service
@19:	  dex
	 bpl @06

	sty NumberOfPlayers
	lda inconsistancy
	bne exit_logon_timer_irq_service

	lda EndLogonRequest	; see if we should end logon
	beq exit_logon_timer_irq_service

	lda #10			; start logon termination process
	sta XmitBuffer+1
	lda #4
	sta logon_state
	bra exit_logon_timer_irq_service


unintelligible_message:
ignore_message:
	stz comlink_semaphore
overflow_collision_service:
	lda logon_state
	cmp #8
	bne @90
	lda EndLogonRequest
	beq @00
	jmp finish_logon_next_timeout
@00:	jsr set_timer_500_msec
@90:	jmp randomise


received_message_service:
	lda #1
	tsb comlink_semaphore
	bne overflow_collision_service
	lda RxMsgStat
	bne unintelligible_message
	lda RxMsgSize
	cmp #LOGON_MSG_SIZE
	bne unintelligible_message
	lda RxMsg+(LogonGameNumber-XmitBuffer)
	cmp #<GAME_ID
	bne unintelligible_message
	lda RxMsg+(LogonGameNumber-XmitBuffer)+1
	cmp #>GAME_ID
	bne unintelligible_message
	lda RxMsg
	bit #1
	bne unintelligible_message
	tax
	cpx #maximium_logon_message_number+1
	bcs unintelligible_message
	jmp (@e0,x)
@e0:	.addr receive_normal_logon_message
	.addr end_logon_command


receive_normal_logon_message:
	ldx RxMsg+1
	cpx #MAX_PLAYERS
	bcs unintelligible_message

	ldx logon_state
	jmp (@e1,x)
@e1:	.addr @A2
	.addr I_have_no_player_number
	.addr ignore_message
	.addr ignore_message
	.addr @A2

@A2:	stz logon_state
	lda RxMsg+(ActvPlrMask-XmitBuffer)
	eor ActvPlrMask
 .IF MAX_PLAYERS>8
	bne @11
	lda RxMsg+(ActvPlrMask-XmitBuffer)+1
	eor ActvPlrMask+1
 .ENDIF
	beq @10
@11:	lda #INCONSISTANCY_PRESET	; data disagrees with my view
	sta inconsistancy

@10:	ldx RxMsg+1
	cpx PlayerNumber
	bne @13
	 phx
	  jsr pick_new_player_number
	 plx
	bcc @14
	jmp invalidate_my_player_number	; oops, too many players

@13:	jsr force_active

@14:	ldy #0			; figure out who's next to speak
@04:	  iny
	  inx
	  cpx #MAX_PLAYERS
	  bne @15
	  ldx #0
@15:	  lda PlayerHeard,x
	 beq @04
	cpx PlayerNumber
	bne @16

	jsr randomise	; it's my turn to speak, set timeout 3-12 msec
	and #$3F	; if no gap before me, set timer to $30-$70 *64usec
	adc #$20	; equals 3-7 milliseconds
	cpy #1
	beq @17
	adc #$50	; if gap, set timer to 8-12 msec in case someone fills
@17:	stz timer_high_order_countdown
	sta TIM5CNT
	stz TIM5CTLB	;start timer
	bra @18

; Case someone else's turn to speak

@16:	jsr set_timer_16_32_millisec

@18:	jsr decrement_everyone_else

clear_semaphore_rts:
	stz comlink_semaphore
	rts


start_logon:
	jsr ZeroComlinkVariables	; erase all comlink variables

	lda #8				; start by checking for redeye busy
	sta logon_state

	lda #<GAME_ID			; copy game number to message buffer
	sta LogonGameNumber
	lda #>GAME_ID
	sta LogonGameNumber+1

	ldx #0				; we are now player zero
	jsr set_number_and_force_active

	lda #INCONSISTANCY_PRESET
	sta inconsistancy

	lda #$80			; set status flag
	sta LogonInProgress
	stz NumberOfPlayers		; set initial number of players

 .IF REDEYE_SLOWNESS > 0
	lda #ENABLE_RELOAD+ENABLE_COUNT	; patch message manager for fast logon
	sta Baud_Patch+1
	lda #ENABLE_INT+ENABLE_COUNT+TxToRx_Divider
	sta TxToRx_Patch+1
	lda #ENABLE_INT+ENABLE_COUNT+MSG_GAP_Divider
	sta MSG_GAP_Patch+1
	lda #ENABLE_INT+ENABLE_COUNT+InterMsg_Divider
	sta InterMsg_Patch+1
	lda #TxToRx_TIMEOUT-1
	sta TxToRx_Patch1+1
 .ENDIF ; REDEYE_SLOWNESS > 0

	 php
	  sei
	  clc
	  jsr initMessageManager	; set up message manager

	  lda #<received_message_service	; link into manager vectors
	  sta readMsgVector
	  lda #<overflow_collision_service
	  sta msgOverflowVector
	  sta transmissionErrorVector
	  sta msgTimeoutVector
	  lda #<msgMgrRtn
	  sta interMsgTimeoutVector
	  sta messageSentVector

	  lda #>received_message_service
	  sta readMsgVector+1
	  lda #>overflow_collision_service
	  sta msgOverflowVector+1
	  sta transmissionErrorVector+1
	  sta msgTimeoutVector+1
	  lda #>msgMgrRtn
	  sta interMsgTimeoutVector+1
	  sta messageSentVector+1

	  ; INIT_ONESHOT logon_timer, AUD_64	; set up logon timer
		stz	TIM5CTLA	; inhibit all timer activity

		lda	#255		; preset count far above zero
		sta	TIM5CNT

		lda	#TIMER_DONE	; inhibit down-counting
		sta	TIM5CTLB

		lda	#ENABLE_INT+ENABLE_COUNT+AUD_64
		sta	TIM5CTLA	; put it in one-shot mode
	  lda #<logon_timer_irq_service		; link in our routine
	  sta sysIntTable+(2*logon_timer)
	  lda #>logon_timer_irq_service
	  sta sysIntTable+(2*logon_timer)+1
	  jsr set_timer_500_msec		; and start timer
	 plp
	rts


; = comlink.src ===========================================================
;
; Intermediate level handler for Redeye  --  Handy Software
;

;	.MACRO POWERTABLE
;systemp:	.= 1
;	  #REP ?0
;	    .BY systemp
;systemp:	    .= systemp*2
;	  .endmacro
;	.endmacro

power_of_two_table:
; .IF MAX_PLAYERS>8
;	.hs 0102040810204080
;	.hs 0000000000000000
;	POWERTABLE MAX_PLAYERS-8
; .ELSE
;	POWERTABLE MAX_PLAYERS
; .ENDIF

PlrOffsets:
;systemp:	.= 0
;	#REP MAX_PLAYERS
;	.BY systemp
;systemp: .= systemp+PLAYER_DATA_SIZE
;	.endmacro


; ===========================================================================
;
; The portion of comlink code to handle communication after logon completed

start_comlink:
; starts up the normal comlink communication
	jsr ZeroComlinkVariables

	lda NumberOfPlayers
	bne @30
	rts

@30:	 php
	  sei
	  sec
	  jsr initMessageManager

	  lda #<RcvMsgInt	; set message manager vectors
	  sta readMsgVector
 .IFNDEF REDEYE_ERROR_COUNTS
	  lda #<msgMgrRtn
 .ELSE
	  lda #<incrxfailures
 .ENDIF ;NDEF REDEYE_ERROR_COUNTS
	  sta msgOverflowVector
	  sta transmissionErrorVector
	  sta msgTimeoutVector

	  lda #>RcvMsgInt
	  sta readMsgVector+1
 .IFNDEF REDEYE_ERROR_COUNTS
	  lda #>msgMgrRtn
 .ELSE
	  lda #>incrxfailures
 .ENDIF ;NDEF REDEYE_ERROR_COUNTS
	  sta msgOverflowVector+1
	  sta transmissionErrorVector+1
	  sta msgTimeoutVector+1

	  ldx PlayerNumber
	  bne @00

	  lda #<TimeoutInt	; set vectors needed if master
	  sta interMsgTimeoutVector
	  lda #<ChkResendQueue
	  sta messageSentVector

	  lda #>TimeoutInt
	  sta interMsgTimeoutVector+1
	  lda #>ChkResendQueue
	  sta messageSentVector+1

	  lda #%01000000	; as master, mark that it's ok to send 1st
	  sta OutGoingFlag

@00:	  stz RxMask		; mark that we are waiting for preceding data
 .IF MAX_PLAYERS>8
	  stz RxMask+1
 .ENDIF
@01:	    sec
	    rol RxMask
 .IF MAX_PLAYERS>8
	    rol RxMask+1
 .ENDIF
	    dex
	   bpl @01

	  ldx NumberOfPlayers	; mark all "received data" as bad
	  lda #%10000000
@10:	    sta PlayerFlag0,x
	    sta PlayerFlag1,x
	    dex
	   bpl @10

 .IFDEF REDEYE_ERROR_COUNTS
	  stz resendreqs
	  stz resendreqs+1
	  stz sendreqs
	  stz sendreqs+1
	  stz rxfailures
	  stz rxfailures+1
	  stz badmessages
	  stz badmessages+1
	  stz lockouts
	  stz lockouts+1
 .ENDIF

	 plp
	rts

 .IFDEF REDEYE_ERROR_COUNTS
incrxfailures:
	inc rxfailures
	bne @90
	inc rxfailures+1
@90:	rts
 .ENDIF ;DEF REDEYE_ERROR_COUNTS


RcvMsgInt:
	lda #1
	tsb comlink_semaphore	; lock out overlapping interrupts
 .IFDEF REDEYE_ERROR_COUNTS
	beq @9a
	inc lockouts
	bne @99
	inc lockouts+1
	bra @99
 .ELSE
	bne @99
 .ENDIF

@9a:	lda RxMsgStat
	beq @00
 .IFDEF REDEYE_ERROR_COUNTS
	inc rxfailures
	bne @90
	inc rxfailures+1
 .ELSE
@91:
 .ENDIF
@90:	stz comlink_semaphore	; garbled message, throw it away
@99:	rts

@00:	lda RxMsgSize
	beq @90
	lda RxMsg	; what type of message is it
	and #%00000111
	cmp #6
 .IFDEF REDEYE_ERROR_COUNTS
	bcc @01
@91:	inc badmessages
	bne @90
	inc badmessages+1
	bra @90
 .ELSE
	bcs @90
 .ENDIF
@01:	sbc #3-1	; cc subtracts one more
	bcc @91
	asl a
	tax
	jmp (@e0,x)

@e0:	.addr RcvData
	.addr RcvSendReq
	.addr RcvResendReq


RcvData:
	lda RxMsgSize	; incoming message right size?
 .IFDEF VAR_SIZE_DATA
	cmp #PLAYER_DATA_SIZE+2
	bcc @00
 .ELSE
	cmp #PLAYER_DATA_SIZE+1
	beq @00
 .ENDIF
@90:
 .IFDEF REDEYE_ERROR_COUNTS
	inc badmessages
	bne @91
	inc badmessages+1
 .ENDIF
@91:	stz comlink_semaphore
	rts

@00:	lda RxMsg	; coming from a valid player?
	and #%01111000
	lsr a
	lsr a
	lsr a
	cmp NumberOfPlayers
	beq @10
	bcs @90

@10:	cmp PlayerNumber	; see if me, if so must be resend from master
	beq @91
	tax
	lda RxMsg	; check sequence of incoming message
	bcc @11
	eor #%10000000
@11:	eor Sequence
	bmi @91

	lda PlayerFlag0,x	; see if already received message
	bit RxMsg
	bpl @12
	lda PlayerFlag1,x
@12:	cmp #0
	beq @30
	lda power_of_two_table,x
	trb RxMask
 .IF MAX_PLAYERS>8
	bne @20
	lda power_of_two_table+8,x
	trb RxMask+1
 .ENDIF
	beq @30

@20:	ldy PlrOffsets,x
	 phx
	  bit RxMsg
	  bmi @25

	  stz PlayerFlag0,x
 .IFDEF VAR_SIZE_DATA
	  lda RxMsgSize
	  dec a
	  sta PlayerDataSize0,x
	  beq @29
 .ENDIF
	  ldx #1
@21:	    lda RxMsg,x
	    sta PlayerData0,y
	    inx
	    iny
 .IFDEF VAR_SIZE_DATA
	    cpx RxMsgSize
 .ELSE
	    cpx #PLAYER_DATA_SIZE+1
 .ENDIF
	   bcc @21
	  bra @29

@25:	  stz PlayerFlag1,x
 .IFDEF VAR_SIZE_DATA
	  lda RxMsgSize
	  dec a
	  sta PlayerDataSize1,x
	  beq @29
 .ENDIF
	  ldx #1
@26:	    lda RxMsg,x
	    sta PlayerData1,y
	    inx
	    iny
 .IFDEF VAR_SIZE_DATA
	    cpx RxMsgSize
 .ELSE
	    cpx #PLAYER_DATA_SIZE+1
 .ENDIF
	   bcc @26

@29:	 plx

@30:	ldy PlayerNumber	; am I the master?
	bne @31
	cpx NumberOfPlayers
	bne @40
	jmp ChkResendQueueAndSend	; yep, see who's next to send

@31:	lda power_of_two_table,y	; am I only one left to send data?
	eor RxMask
	bne @40
 .IF MAX_PLAYERS>8
 	lda power_of_two_table+8,y
	eor RxMask+1
	bne @40
 .ENDIF

	inx
	cpx PlayerNumber	; was message received from previous player?
	bne @40

	jsr TryToSendMyData	; Must be my turn to send data

@40:	stz comlink_semaphore
	rts



RcvSendReq:
	ldx PlayerNumber	; can't be for master
	beq @90

	lda RxMsgSize
	dec a	; cmp #1
 .IFDEF REDEYE_ERROR_COUNTS
	beq @00
	inc badmessages
	bne @90
	inc badmessages+1
	bra @90
 .ELSE
	bne @90
 .ENDIF

@00:	lda RxMsg		; request coming for me??
	and #%01111000
	lsr a
	lsr a
	lsr a
	cmp PlayerNumber
	bne @90

 .IFDEF REDEYE_ERROR_COUNTS
	inc sendreqs
	bne @01
	inc sendreqs+1
 .ENDIF

@01:	lda RxMsg		; asking for current frame or previous??
	eor Sequence
	bmi @50

	lda power_of_two_table,x	; am I waiting for data??
	eor RxMask
 .IF MAX_PLAYERS>8
	bne @10
 	lda power_of_two_table+8,x
	eor RxMask+1
 .ENDIF
	beq @20

@10:	jsr AskForResend
	bra @90

@20:	jsr TryToSendMyData
	bra @90

@50:	lda RxMsg
	and #%10000000
	jsr SendPlrXSequenceData
;	bra @90

@90:	stz comlink_semaphore
	rts


RcvResendReq:
	ldx PlayerNumber	; only for master to deal with
	bne @90

@00:	lda RxMsgSize
	cmp #MASK_SIZE+1
 .IFDEF REDEYE_ERROR_COUNTS
	beq @10
@01:	inc badmessages
	bne @90
	inc badmessages+1
	bra @90
 .ELSE
	bne @90
 .ENDIF

 .IFDEF REDEYE_ERROR_COUNTS
@10:	inc resendreqs
	bne @11
	inc resendreqs+1
 .ENDIF

@11:	jsr FigureWhosNext

	lda RxMsg		; if message out of order, don't deal with it
	and #%01111000
	lsr a
	lsr a
	lsr a
	cmp WhosNext
	bne @20

	sta WhosReTxReq		; Set up to resend messages
	lda RxMsg
	and #%10000000
	sta ReTxSequence
	lda RxMsg+1
	sta ReTxMask
 .IF MAX_PLAYERS>8
	lda RxMsg+2
	sta ReTxMask+1
 .ENDIF
	jmp ResendNext		; will clear comlink_semaphore

@20:	jsr AskForSend
;	bra @90

@90:	stz comlink_semaphore
	rts



ChkResendQueue:			; we get here following transmission
	lda LongTimeoutFlag
	beq @00
	jsr startLongTimeout	; if we asked for send, set long timeout
@00:	lda #1
	tsb comlink_semaphore
	bne @91
	lda WhosReTxReq
	bne ResendNext

@90:	stz comlink_semaphore
@91:	rts


TimeoutInt:
	lda #1
	tsb comlink_semaphore
	beq ChkResendQueueAndSend
	jmp startInterMsgTimeout	; restart timer if locked out

ChkResendQueueAndSend:
	lda WhosReTxReq
	bne ResendNext

FigureAndAskForSend:
	jsr FigureWhosNext
	jsr AskForSend

@90:	stz comlink_semaphore
	rts



ResendNext:
	ldx #$ff
	lda ReTxMask
 .IF MAX_PLAYERS>8
	bne @00
	ldx #7
	lda ReTxMask+1
 .ENDIF
	bne @00
	stz WhosReTxReq
	bra FigureAndAskForSend

@00:	  inx
	  lsr a
	 bcc @00

@10:	lda power_of_two_table,x
	trb ReTxMask
 .IF MAX_PLAYERS>8
	lda power_of_two_table+8,x
	trb ReTxMask+1
 .ENDIF
	lda ReTxSequence
	cpx WhosReTxReq
	bcc @11
	eor #%10000000
@11:	jsr SendPlrXSequenceData
	stz comlink_semaphore
	rts


TryToSendMyData:
	lda OutGoingFlag
	bmi @20
	lda #%01000000		; signal that we couldn't send when we wanted
	tsb OutGoingFlag
	rts

@20:	ldx PlayerNumber

 .IFDEF REDEYE_DEBUG
	lda power_of_two_table,x	; make sure we're not waiting for msg
	cmp RxMask
   .IF MAX_PLAYERS>8
 	bne @10
	lda power_of_two_table+8,x
	cmp RxMask+1
   .ENDIF
	beq @00
@10:	brk
 .ENDIF

@00:	lda Sequence
	bmi @01
	stz PlayerFlag0,x	; mark data as sent/received
	bra @02
@01:	stz PlayerFlag1,x

@02:	jsr SendPlrXSequenceData	; send the data

	lda Sequence		; get ready to deal with next sequence
	eor #%10000000
	sta Sequence

	ldx NumberOfPlayers	; mark all players as data needed
	lda power_of_two_table,x
	asl a
	sta RxMask
 .IF MAX_PLAYERS>8
	lda power_of_two_table+8,x
	rol
 .ENDIF
	dec RxMask
 .IF MAX_PLAYERS>8
	bpl @03
	dea
@03:	sta RxMask+1
 .ENDIF

	lda #1			; if master, expect next data from player 1
	sta WhosNext
	stz OutGoingFlag	; mark that we've used our data

	rts


SendPlrXSequenceData:
 .IFDEF VAR_SIZE_DATA
	ldy PlayerDataSize0,x
	bit #%10000000
	beq @01
	ldy PlayerDataSize1,x
@01:	phy
 .ENDIF

	ldy PlrOffsets,x
	sta XmitBuffer
	txa
	ldx #0
	asl a
	asl a
	asl a
	ora #3
	ora XmitBuffer
	sta XmitBuffer
	bmi @10

@00:	  lda PlayerData0,y
	  sta XmitBuffer+1,x
	  iny
	  inx
	  cpx #PLAYER_DATA_SIZE
	 bcc @00
	bra @20

@10:	  lda PlayerData1,y
	  sta XmitBuffer+1,x
	  iny
	  inx
	  cpx #PLAYER_DATA_SIZE
	 bcc @10

@20:	stz LongTimeoutFlag
 .IFDEF VAR_SIZE_DATA
	pla
	inc a
 .ELSE
	lda #PLAYER_DATA_SIZE+1
 .ENDIF
;	jmp sendLengthMessage
	bra sendLengthMessage	; within range


AskForSend:
	lda WhosNext
	bne @10
	jmp TryToSendMyData

@10:
 .IFDEF REDEYE_ERROR_COUNTS
	inc sendreqs
	bne @11
	inc sendreqs+1
 .ENDIF
@11:	asl a
	asl a
	asl a
	ora #4
	ora Sequence
	eor #%10000000
	sta XmitBuffer
	lda #1
	sta LongTimeoutFlag
;	jmp sendLengthMessage
	bra sendLengthMessage		; within range


AskForResend:
 .IFDEF REDEYE_ERROR_COUNTS
	inc resendreqs
	bne @00
	inc resendreqs+1
 .ENDIF

@00:	ldx PlayerNumber
	txa
	asl a
	asl a
	asl a
	ora Sequence
	ora #5
	sta XmitBuffer
	lda power_of_two_table,x	; ask for everyone we don't have
	eor RxMask			; (except ourself, of course)
	sta XmitBuffer+1
 .IF MAX_PLAYERS>8
	lda power_of_two_table+8,x
	eor RxMask+1
	sta XmitBuffer+2
 .ENDIF
	stz LongTimeoutFlag
	lda #MASK_SIZE+1
;	jmp sendLengthMessage		; fall through


sendLengthMessage:
	ldx #<XmitBuffer
	ldy #>XmitBuffer
	cli
	jmp WaitAndSendMsg



launch_redeye:
	lda OutGoingFlag	; system still hasn't sent previous data (?)
	bmi launch_redeye

	ldx PlayerNumber	; copy our data into appropriate buffer
	ldy PlrOffsets,x
	lda Seq
	bmi @12

 .IFDEF VAR_SIZE_DATA
	lda OutGoingSize
	sta PlayerDataSize0,x
	beq @20
 .ENDIF
	ldx #0
@00:	  lda OutGoingData,x
	  sta PlayerData0,y
	  inx
	  iny
@01:
 .IFDEF VAR_SIZE_DATA
	  cpx OutGoingSize
 .ELSE
	  cpx #PLAYER_DATA_SIZE
 .ENDIF
	 bcc @00
	bra @20

@12:
 .IFDEF VAR_SIZE_DATA
	lda OutGoingSize
	sta PlayerDataSize1,x
	beq @20
 .ENDIF
	ldx #0
@10:	  lda OutGoingData,x
	  sta PlayerData1,y
	  inx
	  iny
@11:
 .IFDEF VAR_SIZE_DATA
	  cpx OutGoingSize
 .ELSE
	  cpx #PLAYER_DATA_SIZE
 .ENDIF
	 bcc @10

@20:	lda NumberOfPlayers
	bne @21
	stz PlayerFlag0
	stz PlayerFlag1
	bra @90
	
@21:	lda #1			; pause redeye for a moment
	tsb comlink_semaphore

	lda #%10000000		; tell system our data is good now
	tsb OutGoingFlag

	lda OutGoingFlag	; did we already fail to send our data?
	and #%01111111
	beq @40

 .IFDEF REDEYE_DEBUG
	ldx PlayerNumber	; are we the master?
	bne @30
	lda WhosReTxReq		; are we still doing resends?
	beq @30
	brk			; should never happen
 .ENDIF

@30:	jsr TryToSendMyData	; send our data

@40:	stz comlink_semaphore	; let redeye go

@90:	lda #%10000000
	eor Seq
	sta Seq

	rts


FigureWhosNext:
	ldx WhosNext
@00:	  lda power_of_two_table,x
	  bit RxMask
	  bne @10
 .IF MAX_PLAYERS>8
	  lda power_of_two_table+8,x
	  bit RxMask+1
	  bne @10
 .ENDIF
	  cpx NumberOfPlayers
	  inx
	 bcc @00

	ldx #0
	
@10:	stx WhosNext

	rts


ZeroComlinkVariables:
	;ldx #end_of_comlink_erasable_variables-start_of_comlink_variables
;@00:	  stz start_of_comlink_variables-1,x
;	  dex
;	 bne @00

	rts



;============================================================================
; 
; Comlink Message Manager  --  Handy Software
; 

;---------------------------------------------------------------------------
; MESSAGE MANAGER VARIABLES
;===========================================================================

RxRawStat:		.res  1	; Received Message Status Byte
				; B7 = Msg checksum error
				; B6 = Msg size byte out of range
				; B5 = Msg gap timeout occurred
				; B4 = Byte parity error
				; B3 = Byte overrun error
				; B2 = Byte framing error
				; B1 = 
				; B0 = 

RxRaw:			.res  MAX_DATA_SIZE+2
				; holds raw incoming data

RxByteCounter:		.res  1	; Position of next incoming byte in RxRaw

RxChecksum:		.res  1	; used to accumulate checksum

RxRawSize:		.res  1	; expected number of bytes in current raw msg

RxMsgBusy:		.res  1	; non zero when user is reading message

RxMsg:			.res  MAX_DATA_SIZE+1
				; user's copy of received message (read only)

RxMsgSize:		.res  1	; number of bytes in user's message

RxMsgStat:		.res  1	; Received Message Status Byte (read only)
				; B7 = Msg checksum error
				; B6 = Msg size byte out of range
				; B5 = Msg gap timeout occurred
				; B4 = Byte parity error
				; B3 = Byte overrun error
				; B2 = Byte framing error
				; B1 = 
				; B0 = 


TxRaw:			.res  MAX_DATA_SIZE+2
				; holds raw outgoing data

TxByteCounter:		.res  1	; Position of next outgoing byte in TxRaw

TxRawSize:		.res  1	; number of bytes in current raw msg

TxToRxTmrOn:		.res  1	; Bit(7) Set by transmitter to signal TxToRx
				; Bit(6) Set to signal interMsg timeout
				; All bits clear signals msg gap timeout
				; Cleared by the timer timing out



SERCTL_RAM:		.res  1	; Shadow of writes to SERCTL

COMLINK_STAT:		.res  1  ; condition of the physical link
				; B7 = comlink cable plugged in
				; B6 = 
				; B5 = 
				; B4 = 
				; B3 = 
				; B2 = 
				; B1 = break active
				; B0 = 





;============================================================================
; 
; Comlink low level Message Manager  --  Handy Software
; 

MsgMgrVers		= $0104






;---------------------------------------------------------------------------
; USER ROUTINES CALLED BY MESSAGE MANAGER
;===========================================================================
;===========================================================================
;===========================================================================


; Vector: readMsgVector
;===========================================================================
; This is called when a complete message has been received.
; RxMsgStat, RxMsgSize and RxMsg can then be read.
; (interrupt mask is cleared)


; Vector: msgOverflowVector
;===========================================================================
; This is called when a complete message has been received and you are still
; reading the last one.
; (interrupt mask is cleared)


; Vector: msgTimeoutVector
;===========================================================================
; This is called when a message byte failed to arrive in time. It tells you
; that some data was received but not enough to complete the message.
; (interrupt mask is set)


; Vector: interMsgTimeoutVector
;===========================================================================
; This is called when too much time has passed before a message has started
; to come in.
; (interrupt mask is set)


; Vector: transmissionErrorVector
;===========================================================================
; The handy's receiver always receives what it sent unless somebody else is
; sending at the same time. At the end of a transmission, as soon as the
; last byte has gone out, the byte in the receiver is compared with the last
; byte that was transmitted. If they don't match, this error routine is called
; (interrupt mask is set)





;---------------------------------------------------------------------------
; ROUTINES CALLED BY USER
;===========================================================================
;===========================================================================
;===========================================================================



initMessageManager:
;---------------------------------------------------------------------------
; Call this once to initialize the Message Manager
;===========================================================================

		;-- install comlink interrupt handlers
		php
		sei

		lda #<SerialIrq
		ldx #>SerialIrq

		sta sysIntTable+(4*2)
		stx sysIntTable+(4*2)+1
		lda #<msgMgrTimerIrq
		ldx #>msgMgrTimerIrq
		sta sysIntTable+(COMLINK_TIMER*2)
		stx sysIntTable+(COMLINK_TIMER*2)+1

		plp

		;-- set baud rate
		lda #1
		sta BAUDBKUP

		;-- if REDEYE_SLOWNESS = 0 use 62500 baud,
		;-- if 1 use 31250, if 2 use 15625, if 3 use 7812.5

Baud_Patch:
		lda #ENABLE_RELOAD+ENABLE_COUNT+REDEYE_SLOWNESS
		sta TIM4CTLA

		; INIT_ONESHOT COMLINK_TIMER,AUD_4
		stz	TIM1CTLA 	; inhibit all timer activity

		lda	#255		; preset count far above zero
		sta	TIM1CNT

		lda	#TIMER_DONE	; inhibit down-counting
		sta	TIM1CTLB

		lda	#ENABLE_INT+ENABLE_COUNT+AUD_4
		sta	TIM1CTLA	; put it in one-shot mode

		;-- init this flag
		stz RxMsgBusy

		stz TxToRxTmrOn

		;-- fall thru to resetReceiver


resetReceiver:
		;-- enable the message receiver
		stz RxByteCounter

		;-- read and init the uart
		lda SERDAT
		ldy SERCTL

		ldx #STDCOM+RESETERR+RXINTEN
		stx SERCTL_RAM
		stx SERCTL

		ldx #SERIAL_INTERRUPT
		stx INTRST

		rts





WaitAndSendMsg:
;---------------------------------------------------------------------------
; Wait until last msg has been sent then call SendMsg
;===========================================================================
@11:		bit SERCTL_RAM
		bmi @11
		;-- fall thru to SendMsg



SendMsg:
;---------------------------------------------------------------------------
;
; This routine is called by the user to send a message.
;
; This routine grabs a copy of the user's data, adds length byte and
;    checksum byte, and activates the interrupt driven message transmitter.
;
; Call with:	address of user's data in X,Y.
;		number of bytes of data in A.  (can be from 0 to 254)
;
; Well behaved users will not call SendMsg while SERCTL_RAM(bit7) is set.
;		( see WaitAndSendMsg above. )
;===========================================================================

		;-- patch addr of user data into move routine
		stx @11+1
		sty @11+2

 .IFDEF REDEYE_DEBUG
		cmp #255	;check range of number of bytes
		bcc @b1
		lda #254
		brk		; oops
 .ENDIF

@b1:		sta TxRaw

		tax
		bra @12

		;-- copy user data to TxRaw
@11:		  lda 9999,X	; selfmod
		  sta TxRaw+1,X
@12:		  dex
		  cpx #$ff
		 bne @11

		;-- calculate and store checksum
@22:		ldx TxRaw
		inx
		lda #255
@33:		  sec
		  sbc TxRaw-1,X
		  dex
		 bne @33
		ldx TxRaw
		sta TxRaw+1,X

		;-- init the message transmitter
		inx
		inx
		stx TxRawSize

		lda #1
		sta TxByteCounter

		;-- stop message gap timer
		;STOP_ONESHOT COMLINK_TIMER
		lda	#TIMER_DONE
		sta	TIM1CTLB

		;-- send first byte
		lda TxRaw
		sta SERDAT

		;-- enable Tx and disable Rx interrupts
		php
		sei

		lda #STDCOM+TXINTEN
		sta SERCTL_RAM
		sta SERCTL
		lda SERDAT
		plp

		rts





;---------------------------------------------------------------------------
; INTERNAL CODE
;===========================================================================
;===========================================================================
;===========================================================================



;---------------------------------------------------------------------------
;
; This is the Serial Interrupt handler.
;
;===========================================================================

SerialIrq:
		phy
		bit SERCTL_RAM
		bpl RxIrq


; TxIrq
;---------------------------------------
		ldx TxByteCounter

		;-- check if this was the last byte in the message
		cpx TxRawSize
		beq SerialIrq11

		;-- send next byte
		lda TxRaw,X
		sta SERDAT
		inc TxByteCounter

SerialIrq22:		;-- clear interrupt
		lda #SERIAL_INTERRUPT
		sta INTRST

		ply

		plx
		pla
		rti


SerialIrq11:		;-- notify timer interrupt handler to do turnaround code
		lda #%10000000
		sta TxToRxTmrOn

		;-- reload Tx to Rx turnaround timer
;		START_ONESHOT COMLINK_TIMER,TxToRx_TIMEOUT

TxToRx_Patch1:
	lda #TxToRx_SLOW-1
	sta TIM1CNT
TxToRx_Patch:
	lda #ENABLE_INT+ENABLE_COUNT+TxToRx_Divider+REDEYE_SLOWNESS
	sta TIM1CTLA
	stz TIM1CTLB

		;-- disable TxEmpty interrupts
		lda #STDCOM
		sta SERCTL_RAM
		sta SERCTL

		bra SerialIrq22



RxIrq:
;---------------------------------------

		;-- restart Msg Gap timer
		stz TxToRxTmrOn

	lda #MSG_GAP_TIMEOUT-1
	sta TIM1CNT
MSG_GAP_Patch:
	lda #ENABLE_INT+ENABLE_COUNT+MSG_GAP_Divider+REDEYE_SLOWNESS
	sta TIM1CTLA
	stz TIM1CTLB

		;-- capture serial data and status
		ldx SERDAT
		lda SERCTL

		;-- reset uart error bits
		ldy #STDCOM+RXINTEN+RESETERR
		sty SERCTL

		;-- reset interrupt
		ldy #SERIAL_INTERRUPT
		sty INTRST

		;-- get current byte's reception errors
		and #PARERR+OVERRUN+FRAMERR+RXBRK
		bne @80		; this is bad data


; Good Data
; This code handles bytes received with no errors:

		txa			; get data in A
		ldx RxByteCounter	; is this the first byte of a msg ?
		beq @60

		;-- save received byte
		sta RxRaw,X
		inx
		stx RxByteCounter	; advance data pointer

		;-- is this the last byte of a message ?
		cpx RxRawSize
		bcs @70

@90:		;-- return from serial interrupt

		ply

		plx
		pla
		rti





; First Byte
		;-- advance data pointer
@60:		inc RxByteCounter

		;-- init error accumulator
		stz RxRawStat

		;-- is the size an allowable value ?
		cmp #MAX_DATA_SIZE+1
		bcc @61
		;-- if not notify of size byte error
		lda #MSG_SIZE_ERR
		tsb RxRawStat
		;-- and assume max message size
		lda #MAX_DATA_SIZE
@61:		;-- save data
		sta RxRaw
		inc
		inc
		sta RxRawSize
		bra @90		; start timer and return

; Last Byte
@70:		jsr endOfMsg
		bra @90		; return from interrupt



; Bad Data
; This code handles bytes received with errors:

@80:		bit #RXBRK	; if a break condition don't save the rx data
		bne @90

		;-- accumulate received errors
		tsb RxRawStat

		ldx RxByteCounter	; is this the first byte ?
		bne @81
		ldy #MAX_DATA_SIZE+2	; default to max size
		sty RxRawSize

@81:		bit #OVERRUN	; lost a byte ?
		beq @82
		inx

@82:		cpx RxRawSize	; is this the last byte ?
		bcs @70
		stx RxByteCounter
		bra @90		; start timer and return

;@70:		jsr endOfMsg
;		bra @90		; return from interrupt



endOfMsg:
;---------------------------------------------------------------------------
		;-- stop message gap timer
		;STOP_ONESHOT COMLINK_TIMER
		lda	#TIMER_DONE
		sta	TIM1CTLB

		jsr startInterMsgTimeout

		;-- reset message byte counter
		stz RxByteCounter

		;-- is last msg still being read by user ?
		lda #128
		tsb RxMsgBusy

		beq @11
		cli
		jmp msgMgrRtn	; selfmod  ; let error handler return for us
;msgOverflowVector: .= *-2

		;-- init checksum
@11:		lda RxRaw
		sta RxChecksum

		;-- copy msg data to user's buffer
		ldx RxRawSize
		dex
		dex
		stx RxMsgSize
		inx
@22:		  ;-- copy raw to Msg and evaluate checksum
		  lda RxRaw,X
		  sta RxMsg-1,X
		  clc
		  adc RxChecksum
		  sta RxChecksum
		  dex
		 bne @22

		;-- evaluate checksum
		lda RxChecksum
		inc
		beq @33
		;-- notify of checksum error
		lda #MSG_CHECKSUM_ERR
		tsb RxRawStat
@33:
		;-- copy msg status to user's buffer
		lda RxRawStat
		sta RxMsgStat

		;-- call the higher level handler
		;-- with valid data in RxMsgStat, RxMsgSize and RxMsg
		cli
		jsr msgMgrRtn	; selfmod
;readMsgVector:	.= *-2

		;-- clear RxMsgBusy
@44:		stz RxMsgBusy

msgMgrRtn:	rts

;readMsgVector	= readMsgVector	; defined so they can be forced into
;msgOverflowVector = msgOverflowVector	;  symbol table with +E command switch


startInterMsgTimeout:
		lda PlayerNumber
		bne startInterMsgTimeout1

		;-- notify timer interrupt handler to do timeout code
		lda #%01000000
		sta TxToRxTmrOn

		;-- reload timer
;		START_ONESHOT COMLINK_TIMER,InterMsg_TIMEOUT
	lda #InterMsg_TIMEOUT-1
	sta TIM1CNT
InterMsg_Patch:
	lda #ENABLE_INT+ENABLE_COUNT+InterMsg_Divider+REDEYE_SLOWNESS
	sta TIM1CTLA
	stz TIM1CTLB

startInterMsgTimeout1:		rts


startLongTimeout:
		;-- notify timer interrupt handler to do timeout code
		lda #%01000000
		sta TxToRxTmrOn

		;-- reload timer
;		START_ONESHOT COMLINK_TIMER,InterMsg_TIMEOUT
	lda #Long_TIMEOUT-1
	sta TIM1CNT
	lda #ENABLE_INT+ENABLE_COUNT+Long_Divider
	sta TIM1CTLA
	stz TIM1CTLB

@90:		rts






;---------------------------------------------------------------------------
;
; This is the Message Manager's timer interrupt routine
;
; The timer is used for three things:
;
;	-- Tx to Rx turnaround. ie, reactivating the receiver after the
;	     last bit of a transmission has gone out
;
;	-- Message gap timeout. ie, sensing that so much time has passed
;	     since the last byte was received that the next byte must be
;	     the start of a new message.
;
;	-- Intermessage gap timeout. ie, sensing that so much time has passed
;	     since the last message was received that something must be wrong
;	     in the communication.
;
;===========================================================================

msgMgrTimerIrq:
		phy

		;-- is this a TxToRx, MsgGap, or InterMsg timeout
		bit TxToRxTmrOn
		stz TxToRxTmrOn
		bmi TxToRxTimeout
		bvc MsgGapTimeout


; InterMsgTimeout
;-----------------------------------------------------------------------
		jsr msgMgrRtn	; selfmod
interMsgTimeoutVector = *-2

		bra endTmrIrq


MsgGapTimeout:
;-----------------------------------------------------------------------
		jsr startInterMsgTimeout

		;-- reset message byte counter
		stz RxByteCounter

	;	cli	; don't cli if using system.8 int handler

		;-- notify sender of timeout error
		jsr msgMgrRtn	; selfmod
msgTimeoutVector = *-2

		bra endTmrIrq




TxToRxTimeout:
;-----------------------------------------------------------------------
		jsr startInterMsgTimeout

		jsr resetReceiver	; returns SERDAT in A, SERCTL in Y

		;-- compare last received byte with last transmitted byte
		ldx TxRawSize
		cmp TxRaw-1,X
		bne txErr

		;-- check last received byte's errors
		tya
		bit #PARERR+FRAMERR
		bne txErr
		jsr msgMgrRtn	; selfmod
messageSentVector = *-2

		bra endTmrIrq


txErr:
;-----------------------------------------------------------------------
	;	cli	; don't cli if using system.8 int handler

		;-- notify sender of transmission collision error
		jsr msgMgrRtn	; selfmod
TransmissionErrorVector = *-2

;		bra endTmrIrq	; fall through


endTmrIrq:
;-----------------------------------------------------------------------
		ply

		plx
		pla
		rti



; Tue Sep 18 17:51:17 1990
; === display.src ===========================================================
;
; Display and Color IO Routines --  the 6502 Side of Handy
;

; If either FRAMECOUNT_UP or _DOWN is defined, then declare DisplayFrameCount
	.IFDEF	FRAMECOUNT_UP | FRAMECOUNT_DOWN
DisplayFrameCount:	.res	1
	.ENDIF


	.IFDEF EOF_USER
; - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - -
; - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - -
; - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - -
	;------	If an EOF user, create the elaborate version of the
	;------	end-of-frame handler

InitEOF:
		;------	Copy old vector into our exit JMP instruction
		LDA	sysIntTable+{2*2}
		STA	FrameEndExit+1
		LDA	sysIntTable+{2*2}+1
		STA	FrameEndExit+2
		;------	Install our vector into the interrupt table
		LDA	#<FrameEnd
		STA	sysIntTable+{2*2}
		LDA	#>FrameEnd
		STA	sysIntTable+{2*2}+1
		RTS

FrameEnd:
; End-of-Frame handler
		PHY

		;------	The TIMEOUT macro will do nothing unless
		;------ AUTO_TIMEOUT_USER has been defined in which case the
		;------	system will turn off after a given amount of time
	  .IFNDEF NO_RICOH_PATCH
		lda MAPCTL
		pha
		 ora #SUZY_SPACE
		 sta MAPCTL
		 TIMEOUT
		pla
		sta MAPCTL
	  .ELSE
		TIMEOUT
	  .ENDIF ;NDEF NO_RICOH_PATCH

		;------	Turn on the display, and then EOF flag
;!!! New:  invoke the DISPLAY macro here during EOF processing
		DISPLAY

	  .IFDEF FRAMECOUNT_UP
		;------	Increment the DisplayFrameCount
		INC	DisplayFrameCount
	  .ELSE ; put this .ELSE here to avoid both UP and DOWN being included
	    .IFDEF FRAMECOUNT_DOWN
		;------	Decrement the DisplayFrameCount until it hits zero
		LDA	DisplayFrameCount
		BEQ	@10
		DEC	DisplayFrameCount
@10:
	    .ENDIF
	  .ENDIF

		LDA	DisplayFlags
		ORA	#DISPLAY_EOFFLAG
		STA	DisplayFlags

		PLY
FrameEndExit:
		;------	This vector ought to be patched by the Init code
		JMP	IntReturn


	.ELSE ; of .IFDEF EOF_USER

; - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - -

	;------	Else create a simple version of the end-of-frame handler

InitDisplayer:
		;------	Copy old vector into our exit JMP instruction
		LDA	sysIntTable+(2*2)
		STA	EOFDisplayEndExit+1
		LDA	sysIntTable+(2*2)+1
		STA	EOFDisplayEndExit+2
		;------	Install our vector into the interrupt table
		LDA	#<EOFDisplayEnd
		STA	sysIntTable+(2*2)
		LDA	#>EOFDisplayEnd
		STA	sysIntTable+(2*2)+1
		RTS

EOFDisplayEnd:
; End-of-Frame display handler
		PHY

		;------	The TIMEOUT macro will do nothing unless
		;------ AUTO_TIMEOUT_USER is defined in which case the
		;------ system will turn off after a given amount of time
	  .IFNDEF NO_RICOH_PATCH
		lda MAPCTL
		pha
		 ora #SUZY_SPACE
		 sta MAPCTL
		 TIMEOUT
		pla
		sta MAPCTL
	  .ELSE
		TIMEOUT
	  .ENDIF ;NDEF NO_RICOH_PATCH

		;------	Turn on the display
		DISPLAY
		PLY
EOFDisplayEndExit:
		;------	This vector ought to be patched by the Init code
		;JMP	IntReturn
	.ENDIF	; of .IFDEF EOF_USER

; - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - -




	.IFDEF	EOL_USER
InitEOL:
		;------	Copy old vector into our exit JMP instruction
		LDA	sysIntTable+{0*2}
		STA	LineEndExit+1
		LDA	sysIntTable+{0*2}+1
		STA	LineEndExit+2
		;------	Install our vector into the interrupt table
		LDA	#<LineEnd
		STA	sysIntTable+{0*2}
		LDA	#>LineEnd
		STA	sysIntTable+{0*2}+1
		RTS

LineEnd:
; End-of-Line handler
		PHA
		LDA	DisplayFlags
		ORA	#DISPLAY_EOLFLAG
		STA	DisplayFlags
		PLA
LineEndExit:
		;------	This vector will be patched by the Init code
		JMP	IntReturn

	.ENDIF	; of .IFDEF EOL_USER



; Wednesday 28-Mar-90 12:24:00
; === redeye_glue.src =======================================================
;
; Redeye test program  --  Handy Software
;
;
;	When using this glue code, the variables restartflag and restartcount
; need to be provided.  They do not need initialization, but should not be
; interfered with.  The glue code also expects the arrays joystick, switch,
; oldjoystick, and oldswitch (each MAX_PLAYERS long) to be provided.  The call
; to do_start_comlink will (among other things) initialize these arrays all to
; $FF's and the routine do_comlink_joysticks will copy joystick and switch to
; oldjoystcik and oldswitch, and will get new input from redeye to update the
; joystick and switch arrays.
;
;	The subroutine showlogonscreen should be provided, which will be
; called during the do_logon code.  The subroutine should display a screen
; which provides some feedback to the user about the state of logon, and
; should handle double-buffering.
;
;	The glue code also expects the WAITEOF to be available.
;
;-----------------------------------------------------------------------------

; Glue code constants

LOCALRESET	= %01000000
GLOBALRESET	= %10000000

EOFS_TIL_RESTART = 20	; how long we wait for communication if player tries
; to reset machine - make sure EOFS_TIL_RESTART is bigger than the longest
; possible game frame (allow reset message enough time to get around)

;-----------------------------------------------------------------------------

; Glue code


glue_start_address:


do_logon:
@20:	 lda JOYSTICK	; make sure my buttons/joystick not being pressed
	 bne @20	;  before coming up

	jsr start_logon	; start logon process

@00:	  jsr showlogonscreen	; show a screen during logon
		LDA	#DISPLAY_EOFFLAG
		TRB	DisplayFlags
@waiteof3:
		BIT	DisplayFlags
		BEQ	@waiteof3

	  jsr checklogonover	; see if logon is over
	 bmi @00

	rts


do_start_comlink:
	stz restartflag

	jsr start_comlink	; start up real communication

	ldx #MAX_PLAYERS-1	; start everyone off with all buttons pressed
	lda #$ff		;  so that first frame with real inputs won't
@0:	  sta joystick,x	;   get phantom positive edges
	  sta oldjoystick,x
	  sta switch,x
	  sta oldswitch,x
	  dex
	 bpl @0

	jmp preparejoysticks	; start first redeye transaction before game


do_comlink_joysticks:
	jsr getinput
	lda restartflag
	beq @00
	jmp restart

@00:	ldx PlayerNumber
	jsr checkflip

	jmp checkreset



checkflip:
	lda joystick,x		; see if player is pressing FLIP
	bit #FLIP_BUTTON
	beq @90
	lda switch,x
	bit #PAUSE_SWITCH
	beq @90

	lda oldjoystick,x	; and wasn't pressing FLIP last frame
	bit #FLIP_BUTTON
	beq @00
	lda oldswitch,x
	bit #PAUSE_SWITCH
	bne @90

@00:	FLIP			; signal EOF code to flip display

@90:	rts



getinput:
	ldx #0		; index to player getting input from
	ldy #0		; relative offset to player's input buffer
@00:	  lda joystick,x	; copy last frame's inputs
	  sta oldjoystick,x
	  lda switch,x
	  sta oldswitch,x

@13:	  lda Seq		; are we receiving even or odd sequence?
	  beq @20

@10:	  lda PlayerFlag0,x	; has player's data come in ?
	  bpl @15
	  jsr checklocalreset	; if not, handle possible reset
	  beq @10
		LDA	#DISPLAY_EOFFLAG
		TRB	DisplayFlags
@waiteof1:
		BIT	DisplayFlags
		BEQ	@waiteof1
	  dec restartcount
	  bne @10
	  jmp restart

@15:	  lda PlayerData0,y	; get joystick data
	  sta joystick,x	; & store for program use
	  lda PlayerData0+1,y	; get switch data
	  sta switch,x		; & store for program use
	  lda #$80
	  sta PlayerFlag0,x	; tell redeye it's been used
	  bra @30

@20:	  lda PlayerFlag1,x	;has player's data come in ?
	  bpl @25
	  jsr checklocalreset	; if not, handle possible reset
	  beq @20
		LDA	#DISPLAY_EOFFLAG
		TRB	DisplayFlags
@waiteof2:
		BIT	DisplayFlags
		BEQ	@waiteof2
	  dec restartcount
	  bne @20
	  jmp restart

@25:	  lda PlayerData1,y	; get joystick data
	  sta joystick,x	; & store for program use
	  lda PlayerData1+1,y	; get switch data
	  sta switch,x		; & store for program use
	  lda #$80
	  sta PlayerFlag1,x	; tell redeye it's been used

@30:	  clc
	  tya
	  adc #PLAYER_DATA_SIZE
	  tay
	  cpx NumberOfPlayers
	  inx
	 bcc @00

@50:	lda #LOCALRESET		; if we're getting input, don't reset until
	trb restartflag		;  we can let the other guys know
	lda #EOFS_TIL_RESTART
	sta restartcount

;	jmp preparejoysticks	; fall through


preparejoysticks:
	lda JOYSTICK		; read this unit's joystick
	sta OutGoingData	; store joystick in output buffer
	GETSWITCH
	and #PAUSE_SWITCH
	sta OutGoingData+1	; store switch in output buffer

 .IFDEF VAR_SIZE_DATA
 	ldx PlayerNumber
	ldy #2			; if data is same as frame before last
	lda OutGoingData+1	;  no need to send it again
	cmp oldswitch,x
	bne @00
	dey
	lda OutGoingData
	cmp oldjoystick,x
	bne @00
	dey
@00:	sty OutGoingSize
 .ENDIF

	jsr launch_redeye	; tell Redeye to take it away
	rts


checkreset:
 .IFDEF AUTO_TIMEOUT_USER
	lda #0			; check to see if there is activity on any
	ldx NumberOfPlayers	; players joystick, and if so reset inactivity
@00:	  ora joystick,x	; counter
	  ora switch,x
	  dex
	 bpl @00

	tax
	beq @10

	RESET_TIMEOUT		; reset inactivity power-down timeout
 .ENDIF

@10:	ldx NumberOfPlayers
@11:	  lda joystick,x	; see if player pressing OPTION 1
	  bit #RESTART_BUTTON
	  beq @12
	  lda switch,x		;  and PAUSE at same time
	  bit #PAUSE_SWITCH
	  beq @12
	  lda #GLOBALRESET	; set bit that passes through getinput
	  tsb restartflag
@12:	  dex
	 bpl @11

	rts


checklogonover:
	jsr checklocalreset	; allow player to trigger reset
	beq @00
	jmp restart

@00:	lda joystick		; simple joystick handler while not in game
	sta oldjoystick
	lda switch
	sta oldswitch
	lda JOYSTICK
	sta joystick
	GETSWITCH
	sta switch

	ldx #0			; handle display flip logic
	jsr checkflip

	lda joystick		; see if I am pressing A/B buttons
	and #OUTER_BUTTON|INNER_BUTTON
	beq @01
	lda #$80		; pressing button, request end of logon
	sta EndLogonRequest

@01:	lda LogonInProgress	; return current status of logon
	rts


checklocalreset:
	lda JOYSTICK			; see if I am pressing RESET
	bit #RESTART_BUTTON
	beq @90
	GETSWITCH
	bit #PAUSE_SWITCH
	beq @90
	lda #LOCALRESET		; set local reset bit
	tsb restartflag

@90:	lda restartflag		; return state of restartflag
	rts

glue_end_address:


; === sysdata.src ===========================================================
; 
; System Data Allocations  --  the 6502 Side of Handy
; 
; NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  
; 
; This file should be included before any of the 6502:src files.
; 
; Note that you should .ORG to a non-zero page address before including 
; this file.  This is because this file includes data declarations that 
; don't need to be in zero-page memory.
; 
; NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  NOTE  


; Here are the declarations of the local copies of write-only registers 
; (and read-doesn't-equal-write registers such as SPRSYS).  
; Whenever you want to make a change to one of the hardware registers 
; that's write-only, you should read from this RAM copy, change the value, 
; and then write the value out to both the RAM copy and the hardware register.
SPRSYS_RAM:	.res 1
DISPCTL_RAM:	.res 1
IODIR_RAM:	.res 1


; Display buffer pointers for use by the system display routines
RenderBuffer:	.res 2
DisplayBuffer:	.res 2

; Shadow of INTSET used in IRQ handler
INTSET_RAM:	.res 1


	.IFDEF AUTO_TIMEOUT_USER
TimeoutCount:	.res 3
	.ENDIF


; The DisplayFlags field is a field that may enjoy multiple CPU instruction 
; read-modify-write operations peformed by both interrupt and mainline code.  
; Because of this, any code must make sure that interrupt disable set before 
; beginning a read-modify-write cycle, or use the TSB/TRB instructions.
DisplayFlags:	.res	1
; Here's the definitions of DisplayFlags
DISPLAY_EOFFLAG		= $80	; Set every EOF
DISPLAY_EOLFLAG		= $40	; Set every EOL


	.interruptor redeye_irq

redeye_irq:
	lda	INTSET
	bit	#SERIAL_INTERRUPT
	;bne	SerialIrq
	bit	#TIMER1_INTERRUPT
	;bne	ComlinkIrq
	bit	#TIMER5_INTERRUPT
	;tst	logon_loaded_in_system
	beq	@01
	jmp	logon_timer_irq_service
@01:    clc
	rts


